# refers to 'log_messages.db' in the current directory, with entries 'enum' and 'string', and tables 'error' and 'warning'
# kjalaric@gitlab

import sqlite3
import datetime

headerfile_name = "errorlog_messages.hpp"
sourcefile_name = "errorlog_messages.cpp"
database_loc = "log_messages.db"

headerfile_loc = "{}".format(headerfile_name)
sourcefile_loc = "{}".format(sourcefile_name)


if __name__ == "__main__":
    conn = sqlite3.connect(database_loc)
    c = conn.cursor()
    
    warning_read = c.execute("SELECT enum, string FROM warning")
    warnings = warning_read.fetchall()
    
    error_read = c.execute("SELECT enum, string FROM error")
    errors = error_read.fetchall()
    
    with open(headerfile_loc, "w") as headerfile:
        headerfile.write("/*\n * {}\n *\n * AUTOGENERATED ({})\n */\n\n#pragma once\n\n".format(headerfile_name, datetime.datetime.now().strftime("%Y-%m-%d %H:%M")))
        
        # externs for string arrays
        headerfile.write("extern const char* log_messages_error[{}];\nextern const char* log_messages_warning[{}];\n\n".format(len(errors), len(warnings)))

        # enum classes
        headerfile.write("enum class LogError {\n")
        for e in errors:
            headerfile.write("\t{},\n".format(e[0]))
        headerfile.write("};\n\n")
        
        headerfile.write("enum class LogWarning {\n")
        for w in warnings:
            headerfile.write("\t{},\n".format(w[0]))
        headerfile.write("};\n\n")
            
    with open(sourcefile_loc, "w") as sourcefile:
        sourcefile.write("/*\n * {}\n *\n * AUTOGENERATED ({})\n */\n\n#include \"errorlog_messages.hpp\"\n\n".format(sourcefile_name, datetime.datetime.now().strftime("%Y-%m-%d %H:%M")))
        
        # strings
        sourcefile.write("const char* log_messages_error[{}] = {{\n".format(len(errors)))
        for e in errors:
            sourcefile.write("\t\"{}\",\n".format(e[1]))
        sourcefile.write("};\n\n")
        
        sourcefile.write("const char* log_messages_warning[{}] = {{\n".format(len(warnings)))
        for w in warnings:
            sourcefile.write("\t\"{}\",\n".format(w[1]))
        sourcefile.write("};\n\n")
    
           
            
    
