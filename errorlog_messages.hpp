/*
 * errorlog_messages.hpp
 *
 * AUTOGENERATED (2019-07-12 15:09)
 */

#pragma once

extern const char* log_messages_error[1];
extern const char* log_messages_warning[1];

enum class LogError {
	NoError,
};

enum class LogWarning {
	NoWarning,
};
