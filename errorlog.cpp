/*
 * errorlog.cpp
 *
 * kjalaric@gitlab
 */

#include "errorlog.hpp"
#include <fstream>
#include <cstring>

inline static const char* getMessageForLog(LogError ev) { return log_messages_error[static_cast<int>(ev)]; }
inline static const char* getMessageForLog(LogWarning ev) { return log_messages_warning[static_cast<int>(ev)]; }
inline std::time_t ErrorLog::getTime() { return std::chrono::system_clock::to_time_t(std::chrono::system_clock::now()); }

void ErrorLog::operator()(LogError ev) {
   std::ofstream ofs(log_filename, std::ios_base::app);
   std::time_t t = getTime();
   ofs << std::strtok(std::ctime(&t), "\n") << ": ERROR - " << getMessageForLog(ev) << std::endl;
}

void ErrorLog::operator()(LogWarning ev) {
   std::ofstream ofs(log_filename, std::ios_base::app);
   std::time_t t = getTime();
   ofs << std::strtok(std::ctime(&t), "\n") << ": WARNING - " << getMessageForLog(ev) << std::endl;
}

void ErrorLog::operator()(std::string str) {
   std::ofstream ofs(log_filename, std::ios_base::app);
   std::time_t t = getTime();
   ofs << std::strtok(std::ctime(&t), "\n") << ": GENERAL - " << str << std::endl;
}

