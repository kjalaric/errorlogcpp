/*
 * errorlog.hpp
 *
 * kjalaric@gitlab
 */

#pragma once

#include <chrono>
#include <string>
#include "errorlog_messages.hpp"  // autogen

class ErrorLog {
protected:
   std::time_t getTime();
   std::string log_filename;

public:
   ErrorLog(const std::string& filename) : log_filename(filename) {}
   ~ErrorLog() {}

   void operator()(LogError ev);
   void operator()(LogWarning ev);
   void operator()(std::string str);
};

extern ErrorLog LOG_;  // constructed in main.cpp
