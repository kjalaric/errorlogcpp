/*
 * test for log function
 *
 * kjalaric@gitlab
 */

#include "errorlog.hpp"
#include "errorlog_messages.hpp"
#include <string>

static const std::string log_filename = "log.log";  // local directory

// global objects
ErrorLog LOG_(log_filename);

int main() {
   // test logs
   LOG_("Starting...");
   LOG_(LogError::NoError);
   LOG_(LogWarning::NoWarning);
}
