# errorlog

Error logging utility in c++ with some python/sql. 

Errors/warnings are defined in log\_messages.db and converted into c++ source and header files by generate\_c.py. These are both referred to by errorlog.cpp and errorlog.hpp.

Using the log is achieved by initially making a global LOG_ object (this is referred to in an extern in errorlog.hpp) and calling it when required, e.g. in the form LOG\_(LogError::NoError) or LOG\_(LogWarning::NoWarning).

Example use is in test.cpp.